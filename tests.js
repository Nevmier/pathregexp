const PathRegexp = require('./index');

class Testing {
    static get TESTS() {
        return [
            { template: '/user', right: [ '/user', '/user/' ], fails: [ '/', '/use', '/user/1' ] },
        
            { template: '/user/:id', right: [ '/user/12', '/user/12/', '/user/fgeet', '/user/fswtggew/' ], fails: [ '/user', '/user/', '/user/123/f' ] },
        
            { template: '/user/:(int)id', right: [ '/user/1', '/user/-1', '/user/1/' ], fails: [ '/user/aaa', '/user/12.1' ] },
            { template: '/user/:(unsignedint)id', right: [ '/user/12541', '/user/12541/' ], fails: [ '/user/-12541', '/user/lel', '/user/12541.11' ] },
            { template: '/user/:(minusint)id', right: [ '/user/-1', '/user/-1/' ], fails: [ '/user/1', '/user/a', '/user/1.1' ] },
        
            {template: '/user/:?(int)id', right: [ '/user/1', '/user/1/', '/user', /user/ ], fails: [ '/user/a', '/user/12.1', '/user/1/1' ] }
        ];
    }

    static async createTable() {
        let regTable = [];
        let tempRegExp = null;

        for (let i = 0; i < this.TESTS.length; i++) {
            tempRegExp = await PathRegexp.create(this.TESTS[i].template);

            regTable.push({
                template: this.TESTS[i].template,
                reg: tempRegExp
            });
        }

        return regTable;
    }

    static async testRight(regTable) {
        for (let i = 0; i < regTable.length; i++) {
            for (let j = 0; j < this.TESTS[i].right.length; j++) {
                console.log(regTable[i].template);
                console.log(regTable[i].reg);
                console.log(this.TESTS[i].right[j]);
                console.log(regTable[i].reg.test(this.TESTS[i].right[j]));
                console.log('---------------------------');
            }
        }
    }

    static async testFails(regTable) {
        for (let i = 0; i < regTable.length; i++) {
            for (let j = 0; j < this.TESTS[i].fails.length; j++) {
                console.log(regTable[i].template);
                console.log(regTable[i].reg);
                console.log(this.TESTS[i].fails[j]);
                console.log(regTable[i].reg.test(this.TESTS[i].fails[j]));
                console.log('---------------------------');
            }
        }
    }

    static async testAll() {
        let regTable = await this.createTable();

        this.testRight(regTable).then();
        this.testFails(regTable).then();
    }
}

Testing.testAll().then();